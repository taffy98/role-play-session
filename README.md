# Role Play Session

## Mọi chỉnh sửa vui lòng checkout branch. thank you!

---

## GHI CHÚ:

- dấu ngoặc tròn (): những đoạn văn trong dấu ngoặc tròn sẽ là hành động của nhân vật
- dấu ngoặc vuông []: đạo cụ sẽ xuất hiện trong câu chuyện
- lời thoại của nhân vật sẽ được biểu diễn bằng dấu -
- Chuyển cảnh: ** **
- Nhân vật: {
  1. Người dẫn chuyện : Narrator
  2. Hoàng hậu: Queen
  3. Chiếc gương thần: The mirror
  4. Bạch Tuyết: Snow White
  5. Hoàng Tử: Prince

}
---
------------------------------------------Start-------------------------------------
---

Narrator: - Kính thưa quý ông, quý bà. Sau đây là phần diễn kịch của nhóm tiếng anh 4. Tôi xin trân trọng giới thiệu với mọi người vở kịch: "Hoàng hậu, chuyện chưa từng kể" cùng với dàn diễn viên xinh đẹp của chúng tôi. Hân hạnh giới thiệu:

Narrator: - Mr. XXX trong vai hoàng hậu

Narrator: - Mr. XXX trong vai chiếc gương thần

Narrator: - Mr. XXX trong vai bạch tuyết

Narrator: - Mr. XXX trong vai hoàng tử

Narrator: - và tôi, người dẫn chuyện cho vở kịch ngày hôm nay

Narrator: - Ngày nảy ngày nay, đây là phần chuyện chưa từng được biết đến của câu chuyện nàng bạch tuyết và 7 chú lùn của gimm, phần này tiếp nối câu chuyện khi nàng bạch tuyết sau khi trốn đến nhà của 7 chú lùn để thoát khỏi âm mưu độc ác của hoàng hậu phù thủy xinh đẹp.

Hoàng hậu đứng trước tấm gương chải chuốt, bất kì người phụ nữ nào xinh đẹp như bà đều muốn có được đẹp hơn tất cả.

(Hoàng hậu: vuốt tóc, son môi, thoa phấn, múa 1 đoạn nhẹ nhàng)

(Gương sẽ thực hiện theo hoàng hậu theo chiều ngược lại)

Hoàng hậu: - Gương kia ngự ở trên tường, thế gian ai đẹp được dường như ta.

Gương: - Muôn tâu hoàng hậu, hoàng hậu chính là người đẹp nhất phòng này. nhưng còn bạch tuyết muôn phần đẹp hơn.

(Hoàng hậu diễn những động tác tức giận và làm theo những gì người dẫn chuyện nói kế tiếp theo đúng nghĩa đen)

Narrator: - Sau khi nghe gương thần trả lời, hoàng hậu vô cùng tức giận, bà ấy lao vào 1 căn phòng tối om như mực, chỉ có những ánh đèn nhấp nháy cùng với 1 cái hũ rất to ở giữa. Đây chính là phòng luyện phép mà bà ấy thường dùng.

**_Chuyển cảnh sang căn phòng của hoàng hậu_** [1 label chạy ngang qua có ghi là tới phòng của hoàng hậu]

Hoàng hậu: - Ta sẽ tạo ra 1 loại virus nguy hiểm, không màu không mùi, không vị để giết bạch tuyết. Chỉ có ta mới xứng đáng là người đẹp nhất trên thế giới này, muahahahahaha.

(Hoàng hậu giơ tay phải lên) [1 trái táo xuất hiện trên lòng bàn tay] (Bỏ vào cái hũ)

(Hoàng hậu giơ tay trái lên) [1 hình batman(dơi) xuất hiện trên lòng bàn tay] (Bỏ vào cái hũ)

(Hoàng hậu sẽ múa quạt để t thực hiện nghi thức tẩm độc) [Sau khi thực hiện xong thì hoàng hậu lấy ra 1 cái khẩu trang]

Hoàng hậu: - Đây chính là cái khẩu trang chứa virus cực độc "Corona". Bạch tuyết chỉ cần đeo cái này, sau 14 ngày sẽ trúng độc suốt đời, những tên người lùn cũng bị lây lan theo đường miệng. Ta sẽ  lây nhiễm loại virus này cho toàn vương quốc và cho lâu đài Vũ Hán mở tiệc, những ai có khẩu trang mới cho thể tham dự. hahahahahahaha

(Hoàng hậu làm động tác rắc virus vào không khí)

Narrator: - Hoàng hậu nhanh chóng bỏ cái khẩu trang có tẩm độc vào đầu tiên của hộp khẩu trang rồi cải trang thành 1 bà lão. Sau đó, bà ta đi vào rừng, tìm đến ngôi nhà của bạch tuyết và 7 chú lùn.

**_Chuyển cảnh sang nhà của bạch tuyết_** [label nhà của 7 chú lùn chạy ra]

Narrator: - Bạch Tuyết đang ngồi lượm đậu đen và đậu đỏ trước nhà sau khi những chú lùn ra ngoài chơi.

Bạch tuyết: - Tòa lâu đài kia đang mở tiệc vào đêm nay mà dịch corona đang nghiêm trọng, chỉ những ai đeo khẩu trang mới được đến lâu đài dự tiệc. Mình không có một cái khẩu trang nào hết, huhuhu.

(Bạch tuyết ngồi khóc nứt nở thì hoàng hậu aka bà già xuất hiện)

Hoàng hậu: - Vì sao con khóc, không có khẩu trang đi dự tiệc hả.
Bạch tuyết: - Dạ vâng ạ.

(Đoạn này hoàng hậu sẽ quảng cáo 1 cách chuyên nghiệp)

Hoàng hậu: - Đừng lo, đã có khẩu trang Nhật nhập khẩu, giá khuyến mãi trong mùa dịch này từ 35k chỉ còn 350k, Khẩu trang Codon, khẩu trang cho mọi nhà. Ở đâu có tiệc, ở đó có khẩu trang.

Bạch tuyết: - Nhưng con không có tiền. Những chú lùn không bao giờ cho con tiền để đi quẩy cả.

Hoàng hậu: - Không sao, ta sẽ cho con 1 cái, nhưng cái này chỉ là free trial, con chỉ có thể dùng đến 12h đêm, nếu quá hạn sẽ bị khóa đó. Con muốn mua luôn thì nhập credit card của con vào đây.

[Hoàng hậu lấy ra máy cà thẻ]

Bạch tuyết: Con sẽ dùng thử đến lúc hết tiệc.

(Hoàng Hậu lấy ra cái khẩu trang đầu tiên trong hộp và đưa cho Bạch Tuyết)

Hoàng Hậu: - Nhớ kỹ, nếu con dùng quá 12h đêm, khẩu trang sẽ không biến mất nhưng sẽ gây bệnh cho người đeo vào.

Bạch Tuyết: Con nhớ rồi, giờ thì bà mau đi đi, những chú lùn sắp về rồi

(Bạch Tuyết vẫy tay đuổi bà lão ra khỏi nhà)

**Chuyển cảnh sang lâu đài**[label lâu đài chạy qua]

Narrator: - Lâu đài Vũ Hán mở tiệc, những âm thanh sống động, ánh sáng lung linh cùng đoàn người đông đảo vào trong lâu đài.
Bạch Tuyết cũng nhanh chóng mặc bộ váy thật đẹp và đeo khẩu trang trial vào để tham gia dự tiệc.

Narrator: - Hoàng tử chú ý ngay đến bạch tuyết - người đeo khẩu trang phong cách sành điệu kia làm những động tác khiêu khích chàng.

(Hoàng tử đi lại chỗ bạch tuyết, mời nàng khiêu vũ, sau đó cùng nhảy vinahouse theo nhạc sôi động).

(Hoàng tử ôm eo bạch tuyết, chuẩn bị cởi khẩu trang nàng ra để hôn thì tiếng chuông đồng hồ báo hiệu 12h đã tới)

(Bạch tuyết nhanh chóng dùng vũ lực để lôi hoàng tử ra khỏi người rồi vùng vằng chạy đi, không may khẩu trang bị giật rơi ra, nàng cắm đầu cắm cổ chạy theo kiểu ninja)

(Hoàng tử người còn ê ẩm nhặt khẩu trang lên, hít 1 hơi thật biến thái rồi mê mẫn ngất đi)

**Chuyển cảnh sang cuộc thi đeo khẩu trang do Hoàng tử tổ chức** [label cuộc thi chạy qua]

Narrator: - Bạch tuyết, hoàng hậu cùng những người khác xuất hiện, lần lượt lên thử khẩu trang. Thứ tự là hoàng hậu rồi đến bạch tuyết. Hoàng hậu không hề hay biết khẩu trang đó đã bị nhiễm corona nên nhiệt tình để thử, mong lấy đc hoàng tử trẻ tuổi, khỏe mạnh.

Narrator: - Tuy nhiên, hoàng hậu không phải là người đăng ký trial nên khẩu trang không chấp nhận, cho đến khi bạch tuyết thử thì khẩu trang kêu lên.

(Hoàng tử ôm chầm lấy bạch tuyết và hít lấy hít để)

(Hoàng hậu thấy vậy mới tức quá, phát bệnh. Phun một luồng nước miếng vào phía bạch tuyết)

(bạch tuyết và hoàng tử né không kịp, chỉ biết đứng nhìn thì bỗng từ đâu chiếc gương xuất hiện, che chắn cho bạch tuyết)

Bạch tuyết: - Khôngggggggggggggggggg

Gương: - Nàng luôn là người ta yêu nhất, bạch tuyết.

(Gương nói xong thì ho 1 tiếng rồi lăn đùng ra đất)

(cùng lúc đò, ánh sáng chập chờn, Bạch tuyết bỗng hóa thân thành virus chúa, lây nhiễm cho toàn bộ thế giới rồi biến mất.
)
---
## Hết chuyện

